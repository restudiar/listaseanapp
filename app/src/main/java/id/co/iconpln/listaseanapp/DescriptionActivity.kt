package id.co.iconpln.listaseanapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.co.iconpln.listaseanapp.model.Asean
import kotlinx.android.synthetic.main.activity_description.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DescriptionActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_ASEAN = "extra_asean"
    }

    private lateinit var asean: Asean

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_description)

        setupActionBar()
        initIntentExtra()
        showDescription()

        btnDescShare.setOnClickListener {
            val sharedAsean =
                "Nama Negara :\n${asean.name} \n\nDeskripsi Negara :\n${asean.desc} \n\n" +
                        "Gambar Bendera :\n${asean.image} \n\nGambar Simbol :\n${asean.symbol}"
            val sharedAseanIntent = Intent(Intent.ACTION_SEND)
            sharedAseanIntent.putExtra(Intent.EXTRA_TEXT, sharedAsean)
            sharedAseanIntent.type = "text/plain"
            val shareIntent = Intent.createChooser(sharedAseanIntent, null)
            if (shareIntent.resolveActivity(packageManager) != null) {
                startActivity(shareIntent)
            }
        }
    }

    private fun initIntentExtra() {
        asean = intent.getParcelableExtra(EXTRA_ASEAN)
    }

    private fun setupActionBar() {
        supportActionBar?.title = "Description ASEAN Country"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun showDescription() {
        tvDescName.text = asean.name
        tvDescValue.text = asean.desc
        Glide.with(this)
            .load(asean.image)
            .apply(
                RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .error(R.drawable.ic_launcher_foreground)
            )
            .into(ivDescImage)
        Glide.with(this)
            .load(asean.symbol)
            .apply(
                RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .error(R.drawable.ic_launcher_foreground)
            )
            .into(ivDescSymbol)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> {
                false
            }
        }
    }
}
