package id.co.iconpln.listaseanapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns.EMAIL_ADDRESS
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLoginClick.setOnClickListener {
            if (etLoginEmail.text.toString() == "user@mail.com" && etLoginPassword.text.toString() == "password") {
                val mainIntent = Intent(this, MainActivity::class.java)
                startActivity(mainIntent)
                tvLoginStatus.text = getString(R.string.login_berhasil)
                Toast.makeText(this, getString(R.string.login_berhasil), Toast.LENGTH_LONG).show()
                finish()
            } else {
                isEmpty()
            }
        }
    }

    private fun isEmpty() {
        when {
            etLoginEmail.text.toString().isBlank() -> {
                etLoginEmail.setError(getString(R.string.empty_field))
            }
            etLoginPassword.text.toString().isBlank() -> {
                etLoginPassword.setError(getString(R.string.empty_field))
            }
            !EMAIL_ADDRESS.matcher(etLoginEmail.text.toString()).matches() -> {
                etLoginEmail.setError(getString(R.string.worng_format_email))
            }
            etLoginPassword.text.toString().length < 7 -> {
                etLoginPassword.setError(getString(R.string.minimal_password))
            }
            else -> {
                tvLoginStatus.text = getString(R.string.login_gagal)
            }
        }
    }
}
