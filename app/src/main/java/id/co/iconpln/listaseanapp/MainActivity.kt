package id.co.iconpln.listaseanapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ListView
import android.widget.Toast
import id.co.iconpln.listaseanapp.`object`.AseanData
import id.co.iconpln.listaseanapp.adapter.ListViewAseanAdapter
import id.co.iconpln.listaseanapp.model.Asean
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val listAsean: ListView
        get() = lv_list_asean

    private val list: ArrayList<Asean> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupActionBar()
        loadListBaseAdapter(this)
        setListItemClickListener(listAsean)
    }

    private fun setupActionBar() {
        supportActionBar?.title = "List of ASEAN Country"
    }

    private fun loadListBaseAdapter(context: Context) {
        list.addAll(AseanData.listDataAsean)

        val baseAdapter =
            ListViewAseanAdapter(
                context,
                list
            )
        listAsean.adapter = baseAdapter
    }

    private fun setListItemClickListener(listView: ListView) {
        listView.onItemClickListener =
            AdapterView.OnItemClickListener { adapterView, view, i, l ->
                Toast.makeText(this@MainActivity, "Negara ${list[i].name}", Toast.LENGTH_LONG)
                    .show()
                showDescription(list[i])
            }
    }

    private fun showDescription(asean: Asean) {
        val descriptionIntent = Intent(this, DescriptionActivity::class.java)
        descriptionIntent.putExtra(DescriptionActivity.EXTRA_ASEAN, asean)
        startActivity(descriptionIntent)
    }
}
