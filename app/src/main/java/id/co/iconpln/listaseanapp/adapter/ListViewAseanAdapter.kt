package id.co.iconpln.listaseanapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.co.iconpln.listaseanapp.R
import id.co.iconpln.listaseanapp.model.Asean

class ListViewAseanAdapter(val context: Context, val listAsean: ArrayList<Asean>) : BaseAdapter() {
    override fun getView(index: Int, view: View?, viewGroup: ViewGroup?): View {
        val viewLayout = LayoutInflater.from(context)
            .inflate(R.layout.item_list_asean, viewGroup, false)

        val viewHolder = ViewHolder(viewLayout)
        val asean = getItem(index) as Asean
        viewHolder.bind(context,asean)

        return viewLayout
    }

    override fun getItem(index: Int): Any {
        return listAsean[index]
    }

    override fun getItemId(index: Int): Long {
        return index.toLong()
    }

    override fun getCount(): Int {
        return listAsean.size
    }

    private inner class ViewHolder(view: View) {
        private val tvAseanName: TextView = view.findViewById(R.id.tv_asean_name)
        private val tvAseanDesc: TextView = view.findViewById(R.id.tv_asean_desc)
        private val ivAseanImage: ImageView = view.findViewById(R.id.iv_asean_image)
        private val ivAseanSymbol: ImageView = view.findViewById(R.id.iv_asean_symbol)

        fun bind(context: Context, asean: Asean) {
            tvAseanName.text = asean.name
            tvAseanDesc.text = asean.desc

            Glide.with(context)
                .load(asean.image)
                .apply(
                    RequestOptions()
                        .placeholder(R.drawable.ic_launcher_foreground)
                        .error(R.drawable.ic_launcher_foreground)
                )
                .into(ivAseanImage)

            Glide.with(context)
                .load(asean.symbol)
                .apply(
                    RequestOptions()
                        .placeholder(R.drawable.ic_launcher_foreground)
                        .error(R.drawable.ic_launcher_foreground)
                )
                .into(ivAseanSymbol)
        }
    }
}