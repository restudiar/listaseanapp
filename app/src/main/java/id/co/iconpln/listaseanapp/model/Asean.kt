package id.co.iconpln.listaseanapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Asean(
    var name: String = "",
    var desc: String = "",
    var image: String = "",
    var symbol: String = ""
) : Parcelable