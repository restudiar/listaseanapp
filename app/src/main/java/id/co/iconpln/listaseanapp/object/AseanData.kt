package id.co.iconpln.listaseanapp.`object`

import id.co.iconpln.listaseanapp.model.Asean

object AseanData {

    val listDataAsean: ArrayList<Asean>
        get() {
            val list = ArrayList<Asean>()

            for (data in dataAsean) {
                val asean = Asean()
                asean.name = data[0]
                asean.desc = data[1]
                asean.image = data[2]
                asean.symbol = data[3]
                list.add(asean)
            }
            return list
        }

    private var dataAsean = arrayOf(
        arrayOf(
            "Brunei Darussalam",
            "Negara berdaulat di Asia Tenggara yang terletak di pantai utara pulau Kalimantan. Negara ini memiliki wilayah seluas 5.765 km² yang menempati pulau Borneo dengan garis pantai seluruhnya menyentuh Laut Cina Selatan. Wilayahnya dipisahkan ke dalam dua negara bagian di Malaysia yaitu Sarawak dan Sabah.",
            "https://cdn.countryflags.com/thumbs/brunei/flag-800.png",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/Emblem_of_Brunei.svg/274px-Emblem_of_Brunei.svg.png"
        ),
        arrayOf(
            "Filipina",
            "Negara republik di Asia Tenggara, sebelah utara Indonesia, dan Malaysia. Filipina merupakan sebuah negara kepulauan yang terletak di Lingkar Pasifik Barat, negara ini terdiri dari 7.641 pulau. Selama ribuan tahun, warga kepulauan Filipina, dan pekerja keras ini telah mengembangkan sistem cocok tanam Padi yang sangat maju, yang menyediakan makanan pokok bagi masyarakatnya.",
            "https://cdn.countryflags.com/thumbs/philippines/flag-800.png",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Coat_of_arms_of_the_Philippines.svg/216px-Coat_of_arms_of_the_Philippines.svg.png"
        ),
        arrayOf(
            "Indonesia",
            "Negara di Asia Tenggara yang dilintasi garis khatulistiwa dan berada di antara daratan benua Asia dan Australia, serta antara Samudra Pasifik dan Samudra Hindia. Indonesia adalah negara kepulauan terbesar di dunia yang terdiri dari 17.504 pulau. Nama alternatif yang biasa dipakai adalah Nusantara. Dengan populasi Hampir 270.054.853 jiwa pada tahun 2018, Indonesia adalah negara berpenduduk terbesar keempat di dunia dan negara yang berpenduduk Muslim terbesar di dunia, dengan lebih dari 230 juta jiwa.",
            "https://cdn.countryflags.com/thumbs/indonesia/flag-800.png",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/National_emblem_of_Indonesia_Garuda_Pancasila.svg/221px-National_emblem_of_Indonesia_Garuda_Pancasila.svg.png"
        ),
        arrayOf(
            "Kamboja",
            "Negara berbentuk monarki konstitusional di Asia Tenggara. Negara ini merupakan penerus Kekaisaran Khmer yang pernah menguasai seluruh Semenanjung Indochina antara abad ke-11 dan 14.",
            "https://cdn.countryflags.com/thumbs/cambodia/flag-800.png",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1e/Royal_arms_of_Cambodia.svg/210px-Royal_arms_of_Cambodia.svg.png"
        ),
        arrayOf(
            "Laos",
            "Negara yang terkurung daratan di Asia Tenggara, berbatasan dengan Myanmar dan Republik Rakyat Tiongkok di sebelah barat laut, Vietnam di timur, Kamboja di selatan, dan Thailand di sebelah barat. Dari abad ke-14 hingga abad ke-18, negara ini disebut Lan Xang atau Negeri Seribu Gajah.",
            "https://cdn.countryflags.com/thumbs/laos/flag-800.png",
            "https://www.heraldry-wiki.com/heraldrywiki/images/0/04/Laos.png"
        ),
        arrayOf(
            "Malaysia",
            "Negara federal yang terdiri dari tiga belas negeri (negara bagian) dan tiga wilayah federal di Asia Tenggara dengan luas 329.847 km persegi. Ibu kotanya adalah Kuala Lumpur, sedangkan Putrajaya menjadi pusat pemerintahan federal. Jumlah penduduk negara ini mencapai 30.697.000 jiwa pada tahun 2015.",
            "https://cdn.countryflags.com/thumbs/malaysia/flag-800.png",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Coat_of_arms_of_Malaysia.svg/320px-Coat_of_arms_of_Malaysia.svg.png"
        ),
        arrayOf(
            "Myanmar",
            "Negara berdaulat di Asia Tenggara. Myanmar berbatasan dengan India dan Bangladesh di sebelah barat, Thailand dan Laos di sebelah timur dan China di sebelah utara dan timur laut. Negara seluas 676.578 km² ini telah diperintah oleh pemerintahan militer sejak kudeta tahun 1988. Negara ini adalah negara berkembang dan memiliki populasi lebih dari 51 juta jiwa (sensus 2014). Ibu kota negara ini sebelumnya terletak di Yangon sebelum dipindahkan oleh pemerintahan junta militer ke Naypyidaw pada tanggal 7 November 2005.",
            "https://cdn.countryflags.com/thumbs/myanmar/flag-800.png",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/State_seal_of_Myanmar.svg/293px-State_seal_of_Myanmar.svg.png"
        ),
        arrayOf(
            "Singapura",
            "Negara pulau di lepas ujung selatan Semenanjung Malaya, 137 kilometer (85 mi) di utara khatulistiwa di Asia Tenggara. Negara ini terpisah dari Malaysia oleh Selat Johor di utara, dan dari Kepulauan Riau, Indonesia oleh Selat Singapura di selatan. Singapura adalah pusat keuangan terdepan ketiga di dunia dan sebuah kota dunia kosmopolitan yang memainkan peran penting dalam perdagangan dan keuangan internasional. Pelabuhan Singapura adalah satu dari lima pelabuhan tersibuk di dunia.",
            "https://cdn.countryflags.com/thumbs/singapore/flag-800.png",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Coat_of_arms_of_Singapore.svg/290px-Coat_of_arms_of_Singapore.svg.png"
        ),
        arrayOf(
            "Thailand",
            "Negara di Asia Tenggara yang berbatasan dengan Laos dan Kamboja di timur, Malaysia dan Teluk Siam di selatan, dan Myanmar dan Laut Andaman di barat. Kerajaan Thai dahulu dikenal sebagai Siam sampai tanggal 11 Mei 1949. Kata Thai (ไทย) berarti kebebasan dalam bahasa Thai, tetapi juga dapat merujuk kepada suku Thai, sehingga menyebabkan nama Siam masih digunakan di kalangan warga negara Thai terutama kaum minoritas Tionghoa dan Amerika.",
            "https://cdn.countryflags.com/thumbs/thailand/flag-800.png",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Emblem_of_Thailand.svg/226px-Emblem_of_Thailand.svg.png"
        ),
        arrayOf(
            "Vietnam",
            "Negara paling timur di Semenanjung Indochina di Asia Tenggara. Vietnam berbatasan dengan Republik Rakyat Tiongkok di sebelah utara, Laos di sebelah barat laut, Kamboja di sebelah barat daya dan di sebelah timur terbentang Laut China Selatan.",
            "https://cdn.countryflags.com/thumbs/vietnam/flag-800.png",
            "https://www.heraldry-wiki.com/heraldrywiki/images/f/f0/Vietnam.png"
        ),
        arrayOf(
            "Papua Nugini",
            "Negara yang terletak di bagian timur Pulau Papua dan berbatasan darat dengan Provinsi Papua (Indonesia) di sebelah barat. Benua Australia di sebelah selatan dan negara-negara Oseania berbatasan di sebelah selatan, timur, dan utara. Ibu kotanya, dan salah satu kota terbesarnya, adalah Port Moresby.",
            "https://cdn.countryflags.com/thumbs/papua-new-guinea/flag-800.png",
            "https://www.heraldry-wiki.com/heraldrywiki/images/c/c2/Papuang.png"
        ),
        arrayOf(
            "Timor Leste",
            "Negara kecil di sebelah utara Australia dan bagian timur pulau Timor. Selain itu wilayah negara ini juga meliputi pulau Kambing atau Atauro, Jaco, dan enklave Oe-Cusse Ambeno di Timor Barat.",
            "https://cdn.countryflags.com/thumbs/east-timor/flag-800.png",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Coat_of_arms_of_East_Timor.svg/257px-Coat_of_arms_of_East_Timor.svg.png"
        )
    )
}